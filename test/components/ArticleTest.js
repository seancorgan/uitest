import React from 'react';
import { shallow } from 'enzyme';
import Article from 'components//Article.js';

describe('<Article />', () => {

  let component;
  beforeEach(() => {
    component = shallow(<Article />);
  });

  describe('when rendering the component', () => {

    it('should have a className of "article-component"', () => {
      expect(component.hasClass('article-component')).to.equal(true);
    });
  });
});
