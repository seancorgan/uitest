import bioAPI from 'sources/bioAPI';

describe('Bio API', () => {

  it('It should implement getNews Function', () => {
    expect(bioAPI).to.have.property('getNews');
  });


  it('It should be able to fetch at least some news articles from the API', () => {
    return bioAPI.getNews()
    .then((resp)=> {
      expect(resp.data.news).have.length.above(1);
    })
  });

  it('It should be able to fetch to limit the articles to just 5', () => {
    return bioAPI.getNews(5)
    .then((resp)=> {
      expect(resp.data.news).have.lengthOf(5);
    })
  });

});
