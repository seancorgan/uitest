import { UPDATE_OFFSET } from './const';

function action(parameter) {
  return { type: UPDATE_OFFSET, parameter };
}

module.exports = action;
