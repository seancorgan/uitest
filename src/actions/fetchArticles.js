import { FETCH_ARTICLES } from './const';

function action(parameter) {
  return { type: FETCH_ARTICLES, parameter };
}

module.exports = action;
