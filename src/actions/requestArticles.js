import fetchArticles from './fetchArticles';
import fetchArticlesSuccess from './fetchArticlesSuccess';
import fetchArticlesFailed from './fetchArticlesFailed';
import bioAPI from 'sources/bioAPI';

function action(limit, offset) {
  return (dispatch) => {
    dispatch(fetchArticles());
    bioAPI.getNews(limit, offset)
    .then((resp) => {
      if (resp.data.news) {
        dispatch(fetchArticlesSuccess(resp.data.news));
      } else {
        dispatch(fetchArticlesFailed('Failed to find any news articles'));
      }
    })
    .catch((err) => {
      dispatch(fetchArticlesFailed(err));
    });
  };
}

module.exports = action;
