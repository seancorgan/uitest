/* Exports all the actions from a single point.

Allows to import actions like so:

import {action1, action2} from '../actions/'
*/
/* Populated by react-webpack-redux:action */
import updateOffset from '../actions/updateOffset.js';
import fetchArticlesSuccess from '../actions/fetchArticlesSuccess.js';
import fetchArticlesFailed from '../actions/fetchArticlesFailed.js';
import requestArticles from '../actions/requestArticles.js';
import fetchArticles from '../actions/fetchArticles.js';
const actions = {
  fetchArticles,
  requestArticles,
  fetchArticlesFailed,
  fetchArticlesSuccess,
  updateOffset
};
module.exports = actions;
