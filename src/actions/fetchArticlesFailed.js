import { FETCH_ARTICLES_FAILED } from './const';

function action(parameter) {
  return { type: FETCH_ARTICLES_FAILED, parameter };
}

module.exports = action;
