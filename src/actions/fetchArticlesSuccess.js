import { FETCH_ARTICLES_SUCCESS } from './const';

function action(parameter) {
  return { type: FETCH_ARTICLES_SUCCESS, parameter };
}

module.exports = action;
