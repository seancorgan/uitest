import axios from 'axios';

const bioAPI = {
  apiUrl: 'http://www.stellarbiotechnologies.com/media/press-releases/json',
  /**
   * @param limit {int} - limit news by number
   * @param offet {offset} - limit offset by number
   * @return {array} - returns an array of news articles.
   */
  getNews(limit = 100, offset = 0) {
    return axios.get(this.apiUrl, {
      params: {
        limit,
        offset
      }
    });
  }
};

export default bioAPI;
