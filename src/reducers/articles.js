import _ from 'lodash';
/* Define your initial state here.
 *
 * If you change the type from object to something else, do not forget to update
 * src/container/App.js accordingly.
 */
import {
  FETCH_ARTICLES,
  REQUEST_ARTICLES,
  FETCH_ARTICLES_FAILED,
  FETCH_ARTICLES_SUCCESS,
  UPDATE_OFFSET
} from '../actions/const';

const initialState = {
  isFetching: false,
  articles: [],
  offset: 0
};

function reducer(state = initialState, action) {
  /* Keep the reducer clean - do not mutate the original state. */
  // const nextState = Object.assign({}, state);

  switch (action.type) {
    case FETCH_ARTICLES: {
      return _.extend({}, state, {
        isFetching: true
      });
    }
    case REQUEST_ARTICLES: {
      return _.extend({}, state, {
        isFetching: true
      });
    }
    case FETCH_ARTICLES_SUCCESS: {
      return _.extend({}, state, {
        isFetching: false,
        articles: state.articles.concat(action.parameter)
      });
    }
    case FETCH_ARTICLES_FAILED: {
      return _.extend({}, state, {
        isFetching: false
      });
    }
    case UPDATE_OFFSET: {
      return _.extend({}, state, {
        offset: state.offset + action.parameter
      });
    }
    default: {
      /* Return original state if no actions were consumed. */
      return state;
    }
  }
}

module.exports = reducer;
