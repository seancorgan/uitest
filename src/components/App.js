import React from 'react';
import './app.css';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Article from './Article';
import AppBar from 'material-ui/AppBar';
import CircularProgress from 'material-ui/CircularProgress';

class AppComponent extends React.Component {
  componentDidMount() {
    window.addEventListener('scroll', () => {
      this.handleScroll();
    });
  }
  componentWillUnmount() {
    window.removeEventListener('scroll', () => {
      this.handleScroll();
    });
  }
  /**
   * if user reached the bottom of the window and
   * we are not currently fetching articles. load 6 more articles.
   */
  handleScroll() {
    if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight
    && !this.props.articles.isFetching) {
      this.props.actions.updateOffset(6);
      this.props.actions.requestArticles(6, this.props.articles.offset);
    }
  }
  render() {
    let articles = [];
    this.props.articles.articles.forEach((article, key) => {
      articles.push(
        <Article title={article.title} key={key} publishedDate={article.published} />);
    });
    return (
      <MuiThemeProvider>
        <div className="index" ref="index">
          <AppBar
            title="InCrowd UI Test - Sean Corgan"
            iconClassNameRight="muidocs-icon-navigation-expand-more"
          />
          <div className="articles">
            <div className="grid">
              {articles}
              {this.props.articles.isFetching ?
                <CircularProgress className="items-loading" size={1.5} />
                : ''}
            </div>
          </div>
        </div>
      </MuiThemeProvider>
    );
  }
}

AppComponent.propTypes = {
  articles: React.PropTypes.object,
  store: React.PropTypes.object,
  actions: React.PropTypes.object
};

AppComponent.defaultProps = {
};

export default AppComponent;
