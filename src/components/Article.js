import React from 'react';
import './article.css';
import {Card, CardText} from 'material-ui/Card';
import moment from 'moment';

class Article extends React.Component {
  render() {
    const formatedDate = moment(this.props.publishedDate).format('LLL');
    return (
      <div className="article col-4">
        <Card>
          <CardText>
            <h2 className="card-title">
              {this.props.title}
            </h2>
            <p className="published-date">{formatedDate}</p>
          </CardText>
        </Card>
      </div>
    );
  }
}

Article.displayName = 'Article';

Article.propTypes = {
  title: React.PropTypes.string,
  publishedDate: React.PropTypes.string
};

Article.defaultProps = {};

export default Article;
