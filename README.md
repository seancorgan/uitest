# Thanks for taking a look!
Frameworks/Tech I used

- React ( View Framework)
- Redux ( Flux Framework )
- Material UI (UI framework )
- Yo - React-Redux-Generator  ( Project Scaffolding )
- Chia/Mocha ( unit testing)

Redux was probably way overkill for this project, but I wanted to showcase that i'm proficient in using it with React. The key benefits of
redux come in to play when you have large SPA applications, it allows you to have a single state container, and you can control the entire state of the application using actions.

I choose react mainly because I love it for too many reasons to mention here, but mainly it allows you to componentize your UI, and pass application state to it; Secondly during our interview it was mentioned
that was the direction InCrowd wanted to move in.

I used a Yo generator to scaffold out the project, because it allowed me to quickly create actions/components reducers etc through CLI.

Material-UI is a pretty cool react based UI framework that follows Google's Material UI guidelines.  It let me have a decent looking UI setup quickly.

I chose chai mocha as my JS unit testing framework, mainly because i'm very familiar with its API
